from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, CMakeDeps, cmake_layout
import os


class ProjectNameConan(ConanFile):
    name = "Project"
    version = "0.1"
    license = "MIT"
    author = "Jean-Baptiste Lorteau <jb.lort@protonmail.com>"
    url = "https://gitlab.com/jb.lort/cpp-project-template"
    topics = ("cpp","utility")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "enable_tests": [True, False],
               "build_apps": [True, False]}

    default_options = {"shared": True,
                       "enable_tests": True,
                       "build_apps": False}

    export_sources = "CMakeLists.txt", "src/*", "apps/*", "include/*"

    def requirements(self):
        self.test_requires("catch2/3.5.4")

    def build_requirements(self):
        self.tool_requires("cmake/3.22.6")

    def layout(self):
        cmake_layout(self)

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.user_presets_path = False
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        if not self.conf.get("tools.build:skip_test", default=False):
            test_folder = os.path.join("tests")
            self.run(os.path.join(test_folder, "PROJECT_NAME-test"))

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["PROJECT_NAME"]
