C++ Project Template
====================

This is a simple project template that uses [CMake](https://cmake.org/), the
package manager [Conan](https://conan.io/), and the test library
[Catch](https://github.com/catchorg/Catch2).

Its main purpose is to provide a project structure that is ready to go with
only minimal adjustments depending on the project itself.

The default structure is configured for building a library, as well as
providing an app that uses it.

The folders break down as follows:

```
project
├── CMakeLists.txt
├── conanfile.py
│
├── apps
│   ├── CMakeLists.txt
│   └── main.cpp
│
├── include
│   └── libname
│       └── hello_world.h
├── src
│   ├── CMakeLists.txt
│   └── hello_world.cpp
│
└── tests
    ├── CMakeLists.txt
    └── example_test.cpp
```

## Setup instructions

This project depends on Conan, which needs to be installed prior to using this
template. It uses CMake, but Conan handles this requirement itself.
* [Conan installation instructions](https://docs.conan.io/2/installation.html)

The folder structure can be copied and used directly with only a few minor
adjustments.

1. Modify the name of the project in the root `CMakeLists.txt`.
2. Modify the include folder name

## Recommended build instructions

### Unix/MacOS

From the root of the project:

    conan build .

This will install the dependencies, build the project, and run the tests.
